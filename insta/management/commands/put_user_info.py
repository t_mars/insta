import json
import logging
import re

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from main.models import *

from grab.spider import Spider, Task

class InfoSpider(Spider):
    def prepare(self):
        self.result_counter = 0

    def get_data(self, grab):
        ss = '<script type="text/javascript">window._sharedData = '
        fs = ';</script>'
        body = grab.doc.body
        s = body.index(ss)
        f = body.index(fs, s)
        
        return json.loads(body[s+len(ss):f])
        
    def task_initial(self, grab, task):
        auser = task.get('auser')
        if not auser:
            return

        print 'user', auser.id, auser.username, auser.user_id
        
        data = self.get_data(grab)
        info = None
        try:
            info = data['entry_data']['ProfilePage'][0]['user']
        except:
            auser.is_loaded = False
            auser.save()
            return

        auser.is_loaded = True
        auser.count_media = info['media']['count']
        auser.count_follows = info['follows']['count']
        auser.count_followers = info['followed_by']['count']
        auser.is_private = info['is_private']
        auser.url = info['external_url']
        auser.biography = info['biography']
        auser.fullname = info['full_name']
        auser.loaded_at = timezone.now()

        #vk
        r = re.compile('vk\.com\/[a-z0-9_]+', flags=re.I)
        for k in ['external_url', 'full_name', 'biography']:
            v = info.get(k) or ''
            vk = r.findall(v)
            if vk:
                auser.vk_id = vk[0].lower()
                break

        #media
        if 'nodes' in info['media']:
            auser.media = json.dumps([
                {
                    'code': n['code'],
                    'count_comments': n['comments']['count'],
                    'count_likes': n['likes']['count'],
                    'caption': n.get('caption'),
                    'id': n['id'],
                } 
                for n in info['media']['nodes']
            ])

        print 'user', auser.id, auser.username, auser.user_id
        auser.save()

    def task_generator(self):
        while True:
            ausers = AttractedUser.objects.filter(
                is_loaded=None,
                followed_at=None
            ).order_by('?')
            if ausers.count() == 0:
                return
            ausers = ausers[:1000]
            for auser in ausers:
                yield Task('initial', url='https://instagram.com/%s' % auser.username, auser=auser)

class Command(BaseCommand):
    def handle(self, *args, **options):
        logging.basicConfig(level=logging.DEBUG)

        bot = InfoSpider(thread_number=100)
        bot.run()