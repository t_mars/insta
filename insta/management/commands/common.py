#coding=utf8
from __future__ import division
from collections import defaultdict
from math import log

import sys, os, json, datetime
from main.models import *
from django.utils import timezone
from datetime import timedelta

class TaskPerformer():
    args = '<name...>'
    
    def init(self):
        self.id = sys.argv[2]
        self.task = Task.objects.get(pk=self.id) 
        self.pid = os.getpid()

    def put_status(self,d):
        self.task.save_status_string(d)

    def get(self, *args, **kwargs):
        return self.task.get_argument(*args, **kwargs)


from collections import Iterator
class DataSet(Iterator):
    def __init__(self, query_set, step=1000, limit=None):
        self.query_set = query_set
        self.datas = []
        self.shift = 0
        self.limit = limit
        self.step = step

        if self.limit:
            self.count = min(self.limit, query_set.count())
        else:
            self.count = query_set.count()

        print 'total count data set =',self.count
    
    def pop(self):
        if not len(self.datas):
            self._init_datas()
        
        if not len(self.datas):
            return None
    
        self.count -= 1

        return self.datas.pop()

    def next(self):
        if not len(self.datas):
            self._init_datas()

        if not len(self.datas):
            raise StopIteration
        
        self.count -= 1
    
        return self.datas.pop()

    def _init_datas(self):
        new_shift = self.shift+self.step
        if self.limit:
            new_shift = min(new_shift, self.limit)
        qs = self.query_set[self.shift:new_shift]
        self.datas = list(qs)
        self.shift = new_shift


    def __len__(self):
        return self.count

class Classificator():

    def train(self, users):
        samples = [(self.get_features(user), user.followed_at is not None) for user in users]
        
        count = len(samples)
        ind = 0
        self.classes, self.freq = defaultdict(lambda:0), defaultdict(lambda:0)
        for feats, label in samples:
            print '%d/%d' % (ind, count)
            ind += 1
            self.classes[label] += 1                 # count classes frequencies
            for feat in feats:
                self.freq[label, feat] += 1          # count features frequencies

        for label, feat in self.freq:                # normalize features frequencies
            self.freq[label, feat] /= self.classes[label]
        for c in self.classes:                       # normalize classes frequencies
            self.classes[c] /= len(samples)

    def classify(self, user):
        feats = self.get_features(user)
        return min(self.classes.keys(),              # calculate argmin(-log(C|O))
            key = lambda cl: -log(self.classes[cl]) + \
                sum(-log(self.freq.get((cl,feat), 10**(-7))) for feat in feats))

    def get_features(self, user): 
        return (
            'l%s' % user.count_media,          # get last letter
            'f%s' % user.count_followers,           # get first letter
            's%s' % user.count_follows,           # get second letter
        )