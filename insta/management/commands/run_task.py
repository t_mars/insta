#coding=utf8
import os, logging, pickle, subprocess
from time import sleep
from multiprocessing import Process, Pipe
from StringIO import StringIO

from django.db import transaction
from django.core.management.base import BaseCommand

from main.models import Task
from optparse import make_option

class Command(BaseCommand):
	
	option_list = BaseCommand.option_list + (
        make_option('--task',
            dest='task_id',
            help='Номер таска для запуска',
            metavar="INT"
        ),
        make_option('--kill',
            dest='kill_id',
            help='Номер таска для завершения',
            metavar="INT"
        ),
    )

	def handle(self, *args, **options):
		if options['task_id']:
			try:
				task = Task.objects.get(pk=options['task_id'])
			except:
				return 'Задача не найдена'
			
			if not task.can_start():
				print "Задача %d [%s] невозможна для запуска" % (task.id, task.name.encode('utf8'))
				return

			self.run_task(task)
		
		elif options['kill_id']:
			try:
				task = Task.objects.get(pk=options['kill_id'])
			except:
				return 'Задача не найдена'
			
			self.kill_task(task)
		

		else:
			self.run()
	
	def kill_task(self, task):
		print "Задача %d [%s] УБИТА." % (task.id, task.name.encode('utf8'))
		os.popen(task.get_kill_command())
		os.popen("kill -9 %d" % task.pid)

		task.pid = None
		task.status = Task.KILLED
		task.save()

	def run_task(self, task):
		print "Запуск задачи %d [%s]" % (task.id, task.name.encode('utf8'))

		task.status = Task.EXECUTE
		task.status_string = ""
		task.pid = os.getpid()
		task.save()

		print "Запуск %d [%s]" % (task.id, task.name.encode('utf8'))
		
		try:
			sp = subprocess.Popen(task.get_command().split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			task.out, task.err = sp.communicate()

			print "Завершен"

			if sp.returncode == 0:
				task.status = Task.SUCCESS
			else:
				status = Task.objects.get(pk=task.id).status
				if status == Task.KILL:
					print '!Task.KILLED'
					task.status = Task.KILLED
				else:
					print '!Task.FAILURE'
					task.status = Task.FAILURE
					
			
			task.try_count += 1
			task.pid = None
			task.save()

		except KeyboardInterrupt:
			task.status = Task.KILLED
			task.pid = None
			task.save()

	def run(self):
		# убиваем задачи
		tasks = Task.objects.raw(
			'SELECT * FROM main_task at '
			'WHERE at.status=%d '
			'ORDER BY at.id FOR UPDATE' % (Task.KILL)
		)
		for task in tasks:
			self.kill_task(task)
		
		# проверка 'отвалившихся' задач
		tasks = Task.objects.raw(
			'SELECT * FROM main_task at '
			'WHERE at.status=%d '
			'ORDER BY at.id FOR UPDATE' % (Task.EXECUTE)
		)
		for task in tasks:
			if task.is_active_proc():
				continue
			print "Отвалилась задача %d [%s]" % (task.id, task.name.encode('utf8'))
			task.status = Task.FAILURE
			task.pid = None
			task.save()
			if task.can_start():
				task.restart()
				print "Перезапуск задачи %d [%s]" % (task.id, task.name.encode('utf8'))

		# проверка аварийно завершенных задач
		tasks = Task.objects.raw(
			'SELECT * FROM main_task at '
			'WHERE at.status=%d '
			'ORDER BY at.id FOR UPDATE' % (Task.FAILURE)
		)
		for task in tasks:
			if not task.can_start():
				continue
			task.restart()
			print "Перезапуск задачи %d [%s]" % (task.id, task.name.encode('utf8'))
			
		# запуск новых задач
		tasks = Task.objects.raw(
			'SELECT * FROM main_task at '
			'WHERE at.status IN (%s) '
			'ORDER BY RAND() FOR UPDATE' % ','.join([str(s) for s in [Task.NEW,Task.FAILURE,Task.SUCCESS]])
		)
		for task in tasks:
			if not task.can_start():
				continue
			self.run_task(task)
			return

		print 'Нет задач'