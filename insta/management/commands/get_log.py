from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from main.models import *
from main.client import InstagramClient

from common import TaskPerformer

class Command(TaskPerformer, BaseCommand):
    def handle(self, *args, **options):
        self.init()

        accounts = Account.objects.all()
        for account in accounts:
            print account.username,
            client = InstagramClient(account.username, account.password, account.access_token)
            user = None
            for i in range(5):
                try:
                    user = client.get_user()
                    break
                except:
                    continue

            if not user:
                print 'error'
                continue

            print 'ok'

            self.put_status({'user': account.username})
            
            l = AccountLog()
            l.account = account
            l.count_followers = user.counts['followed_by']
            l.count_follows = user.counts['follows']
            l.save()
