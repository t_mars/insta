from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
import datetime

from main.models import *
from main.service import InstagramService

from common import TaskPerformer

class Command(TaskPerformer, BaseCommand):
    def handle(self, *args, **options):
        self.init()

        try:
            campaign = Campaign.objects.get(name=self.get('name'))
        except:
            print 'no campaign'
            return

        service = InstagramService(campaign.account, campaign)

        actions = Action.objects.filter(
            campaign=campaign,
            created_at__lte=timezone.now()-datetime.timedelta(days=1),
            rollbacked_at=None,
            typ='follow'
        ).order_by('created_at')
        
        count = actions.count()
        ind = 0
        for action in actions:
            ind += 1
            service.rollback(action)

            self.put_status({
                'type': action.typ, 
                'comment': action.comment, 
                'current': ind, 
                'total': count,
            })