from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from main.models import *
from main.service import InstagramService

import random

from common import TaskPerformer, Classificator

class Command(TaskPerformer, BaseCommand):
    def handle(self, *args, **options):
        self.init()

        try:
            self.campaign = Campaign.objects.get(name=self.get('name'))
        except:
            print 'no campaign'
            return

        self.account = self.campaign.account
        self.service = InstagramService(self.account, self.campaign)
        
        self.total = 0
        self.current = 0
        self.success = 0
        self.failure = 0
        
        self.qs = AttractedUser.objects.filter(
            is_loaded=True, 
            is_private=False,
            count_media__gt=2,
            followed_at__isnull=True,
            attracted_at__isnull=True,
            campaign=self.campaign
        )
        self.total = self.qs.count()
        
        while True:
            res = self.run()
            if not res:
                break

    def run(self):
        ausers = self.qs[:100]
        if len(ausers) == 0:
            return False
    
        for auser in ausers:
            self.put_status({
                'username': auser.username,
                'total': self.total, 
                'current': self.current, 
                'success': self.success,
                'failure': self.failure, 
            })

            self.current += 1
            print '%d/%d' % (self.current, self.total)
            
            print auser.username, 

            print auser.id
            print auser.username

            media = auser.get_media()
            if len(media) < 2:
                self.failure += 1
                continue
            
            print 'attract'
            self.success += 1

            media = sorted(media, key=lambda m: m['count_likes'], reverse=True)
            
            for i in range(0, 1):
                m = media.pop(0)
                self.service.like(m['id'], 'https://instagram.com/p/%s/' % m['code'])
            self.service.follow(auser.user_id, auser.username)

            auser.attracted_at = timezone.now()
            # auser.campaign = self.campaign
            auser.save()

        return True