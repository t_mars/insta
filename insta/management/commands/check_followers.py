from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from main.models import *
from main.service import InstagramService

from common import TaskPerformer

class Command(TaskPerformer, BaseCommand):
    def handle(self, *args, **options):
        self.init()
        
        try:
            account = Account.objects.get(username=self.get('account'))
        except:
            print 'no account'
            return

        self.service = InstagramService(account)

        self.success = 0
        self.exist = 0
        self.count = 0

        followers, next_ = self.service.get_user_followers(user_id=account.user_id)
        self.save_followers(followers)
        while next_:
            followers, next_ = self.service.get_user_followers(with_next_url=next_)
            self.save_followers(followers)

    def save_followers(self, followers):
        for follower in followers:
            print '%d' % (self.count), follower.username,
            self.count += 1
            try:
                auser = AttractedUser.objects.get(user_id=follower.id)
            except:
                print 'no'
                continue

            if auser.followed_at == None:
                auser.followed_at = timezone.now()
                auser.save()
                self.success += 1
                print 'yes'
            else:
                self.exist += 1
                print 'exist'

            self.put_status({'current': self.count, 'success': self.success, 'exist': self.exist})
