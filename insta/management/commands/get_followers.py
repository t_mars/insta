import sys

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from main.models import *
from main.service import InstagramService

from common import TaskPerformer

class Command(TaskPerformer, BaseCommand):
    def handle(self, *args, **options):
        self.init()
        
        try:
            account = Account.objects.get(username=self.get('account'))
        except:
            print 'no account',self.get('account')
            sys.exit(1)

        name = self.get('name')
        if not name:
            print 'no name'
            sys.exit(1)

        try:
            campaign = Campaign.objects.get(account=account, name=name)
        except:
            campaign = Campaign()
            campaign.name = name
            campaign.account = account
            campaign.save()
        
        self.service = InstagramService(account, campaign)
        self.campaign = campaign

        if not self.get('user'):
            print 'no user'
            sys.exit(1)

        try:
            user_id = int(self.get('user'))
        except:
            user_id = self.service.get_user_id(self.get('user'))
        
        if user_id is None:
            print 'no user id'
            sys.exit(1)

        try:
            user = self.service.get_user(user_id)
        except:
            print 'no user'
            sys.exit(1)

        self.total = user.counts['followed_by']
        self.current = 0
        self.new = 0

        users, next_ = self.service.get_user_followers(user_id=user_id)
        self.save_users(users)
        while next_:
            users, next_ = self.service.get_user_followers(with_next_url=next_)
            self.save_users(users)
    
    def save_users(self, users):
        for u in users:
            self.current += 1

            qs = AttractedUser.objects \
                .filter(user_id=u.id) \
                .order_by('id')
                
            if qs.count() > 0:
                for a in qs[1:]:
                    a.delete()
                status = False
            else:
                status = True

            if status:
                self.new += 1
                auser = AttractedUser()
                auser.user_id = u.id
                auser.username = u.username
                auser.campaign = self.campaign
                auser.save()

            self.put_status({
                'user': u.username, 
                'status': status, 
                'new': self.new, 
                'current': self.current, 
                'total': self.total,
            })