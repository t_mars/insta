#coding=utf8
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
import datetime

from main.models import *
from main.service import InstagramService

from common import Classificator

class Command(BaseCommand):
    
    def handle(self, *args, **options):
        c = Classificator()
        users = AttractedUser.objects.filter(is_attracted=True)
        
        c.train(users)

        good = users.filter(followed_at__isnull=False)
        bad = users.filter(followed_at__isnull=True)
        
        good_count = 0
        for u in good:
            if c.classify(u):
                good_count += 1
  
        print good_count / float(good.count()) * 100.0

        bad_count = 0
        for u in bad:
            if c.classify(u):
                bad_count += 1
        print bad_count / float(bad.count()) * 100.0

