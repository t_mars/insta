import time
import json
import re

from django.utils import timezone

from main.models import *
from main.client import *

from instagram.bind import InstagramAPIError

class InstagramService:
    def __init__(self, account, campaign=None):
        self.client = InstagramClient(account.username, account.password, account.access_token)
        self.campaign = campaign

    def _wait_for(self, func, *args, **kwargs):
        f = True
        print 'wait', func.__func__.func_name
        while True:
            try:
                r = func(*args, **kwargs)
                if not f:
                    print ''
                return r
            except BanException:
                if f:
                    print func.__func__.func_name,
                    f = False
                print '.'
                time.sleep(10)
            except TimeoutException:
                if f:
                    print func.__func__.func_name,
                    f = False
                print '#'
                time.sleep(1)
            except:
                if f:
                    print func.__func__.func_name,
                    f = False
                print '?'
                time.sleep(1)

    def _exec(self, func, id):
        r = self._wait_for(func, id)
        print func.__func__.func_name,
        if r:
            print 'ok'
        else:
            print 'no'        
        
    def like(self, media_id, link):
        if not self.campaign:
            raise Exception('unknow campaign')
        
        action = Action()
        action.campaign = self.campaign
        action.typ = 'like'
        action.source_id = media_id
        action.comment = link

        self._exec(self.client.like, media_id)
        action.save()
    
    def follow(self, user_id, username):
        if not self.campaign:
            raise Exception('unknow campaign')
        
        action = Action()
        action.campaign = self.campaign
        action.typ = 'follow'
        action.source_id = user_id
        action.comment = 'https://instagram.com/%s/' % username

        self._exec(self.client.follow, user_id)
        action.save()

    def rollback(self, action):
        if action.typ == 'follow':
            self._exec(self.client.unfollow, action.source_id)
            action.rollbacked_at = timezone.now()
            action.save()

        elif action.typ == 'like':
            self._exec(self.client.unlike, action.source_id)
            action.rollbacked_at = timezone.now()
            action.save()

    def __getattr__(self, name):
        if hasattr(self.client, name):
            attr = getattr(self.client, name)
            def wrapper(*args, **kwargs):
                return self._wait_for(attr, *args, **kwargs)
            return wrapper

    def get_user_followers(self, user_id=None, with_next_url=None):
        if user_id:
            return self.client.api.user_followed_by(user_id=user_id)
        elif with_next_url:
            return self.client.api.user_followed_by(with_next_url=with_next_url)