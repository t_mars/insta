from django.shortcuts import render

from main.models import *
import time
from datetime import timedelta
import datetime
def get_timestamp(date):
    return int(time.mktime(date.timetuple()))

def get_plot(data):
    return '[%s]' % ','.join('[%s,%s]' % (x,y) for x,y in data)

def get_range(s, sections=40):
    mn = min(s)
    mx = max(s)
    shift = (mx-mn) / float(sections)
    data = {i:0 for i in range(sections+1)}
    _sum = float(len(s))
    for v in s:
        ind = int((v - mn) / shift)
        data[ind] += 1 / _sum
    
    return '[%s]' % ','.join(
        ['[%d,%f]' % (mn+shift*k, v * 100.0) for k,v in data.items()]
    )

def strategy(request):
    good = AttractedUser.objects.filter(is_attracted=True, followed_at__isnull=False)
    bad = AttractedUser.objects.filter(is_attracted=True, followed_at__isnull=True)
    
    return render(request, 'main/strategy.html', {
        'good_media': get_range([u.count_media for u in good]),    
        'bad_media': get_range([u.count_media for u in bad]),    
        
        'good_followers': get_range([u.count_followers for u in good]),    
        'bad_followers': get_range([u.count_followers for u in bad]),    
        
        'good_follows': get_range([u.count_follows for u in good]),    
        'bad_follows': get_range([u.count_follows for u in bad]),    
    })

def stat(request):
    campaign = Campaign.objects.all()[0]
    
    actions = Action.objects \
        .filter(campaign=campaign) 
        # .filter(created_at__gte=datetime.datetime(2015, 8, 20, 00, 00, 00))
    

    logs = AccountLog.objects \
        .filter(account=campaign.account) 
        # .filter(created_at__gte=datetime.datetime(2015, 8, 20, 00, 00, 00))

    start_date = logs.order_by('created_at')[0].created_at
    finish_date = logs.order_by('-created_at')[0].created_at
    shift = get_timestamp(start_date)

    followers_plot = []
    for l in logs:
        x = get_timestamp(l.created_at) - shift
        y = l.count_followers
        followers_plot.append( (x,y) )

    follows_plot = []
    for l in logs:
        x = get_timestamp(l.created_at) - shift
        y = l.count_follows
        follows_plot.append( (x,y) )

    percent_plot = []
    new_followers_plot = []
    for l in logs:
        x = get_timestamp(l.created_at) - shift
        new_followers = l.count_followers - logs[0].count_followers
        new_follows = actions.filter(created_at__lte=l.created_at,typ='follow').count()
        try:
            y1 = new_followers/float(new_follows) * 100.0 
        except:
            y1 = 0
        percent_plot.append( (x, '%2.2f' % y1) )

        total_minutes = (l.created_at - logs[0].created_at).total_seconds() / 60.0
        try:
            y2 = new_followers / total_minutes * 60
        except:
            y2 = 0

        new_followers_plot.append( (x, '%2.2f' % y2) )


    count = 10
    dateticks = []
    step = (finish_date - start_date).total_seconds() / float(count)
    s = start_date
    for i in range(count):
        dateticks.append( (get_timestamp(s) - shift, s.strftime('"%d.%m<br>%H:%M"')) )
        s += timedelta(seconds=step)

    dates = '{%s}' % ','.join([
        '"%2.2f":"%s"' % (get_timestamp(l.created_at) - shift, l.created_at.strftime('%d.%m %H:%M:%S')) for l in logs
    ])

    return render(request, 'main/stat.html', {
        'campaign': campaign,    
        'followers_plot': get_plot(followers_plot),    
        'follows_plot': get_plot(follows_plot),    
        'percent_plot': get_plot(percent_plot),    
        'new_followers_plot': get_plot(new_followers_plot),    
        'dateticks': get_plot(dateticks),
        'dates': dates,
    })