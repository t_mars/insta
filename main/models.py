#coding=utf8
import subprocess, sys, os
import json

from django.db import models
from django.conf import settings
from django.utils import timezone

from main.client import InstagramClient

class Account(models.Model):
    user_id = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    access_token = models.CharField(max_length=255)

    def __str__(self):
        return self.username
    
class Campaign(models.Model):
    name = models.CharField(max_length=255)
    account = models.ForeignKey(Account)
    description = models.TextField()
    
    def __str__(self):
        return self.name

    def get_stat(self):
        qs = Action.objects.filter(campaign=self)
        
        if not qs.count():
            return None

        s = qs.order_by('created_at')[0].created_at
        f = qs.order_by('-created_at')[0].created_at
        new_follows = qs.filter(typ='follow').count()

        total_minutes = (f-s).total_seconds() / 60.0

        qs = AccountLog.objects.filter(account=self.account)

        if not qs.count():
            return None

        log_s = qs.filter(created_at__gte=s).order_by('created_at')[0]
        log_f = qs.filter(created_at__lte=f).order_by('-created_at')[0]
        
        new_followers = log_f.count_followers - log_s.count_followers
        followers = log_f.count_followers
        follows = log_f.count_follows
        
        return {
            'new_followers': new_followers,
            'new_follows': new_follows,
            'new_followers_per_minute': new_followers / total_minutes,
            'new_follows_per_minute': new_follows / total_minutes,
            'percent': new_followers/float(new_follows) * 100.0,
            'followers': followers,
            'follows': follows,
        }

class AttractedUser(models.Model):
    campaign = models.ForeignKey(Campaign, null=True, blank=True)
    
    user_id = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    
    count_media = models.PositiveIntegerField(default=0)
    count_follows = models.PositiveIntegerField(default=0)
    count_followers = models.PositiveIntegerField(default=0)
    
    is_private = models.NullBooleanField(default=None)
    is_loaded = models.NullBooleanField(default=None)
    fullname = models.CharField(max_length=255, default=None, null=True, blank=True)
    url = models.CharField(max_length=255, default=None, null=True, blank=True)
    vk_id = models.CharField(max_length=255, default=None, null=True, blank=True)
    
    media = models.TextField(default=None, null=True, blank=True)
    biography = models.TextField(default=None, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    loaded_at = models.DateTimeField(null=True, blank=True, default=None)
    attracted_at = models.DateTimeField(null=True, blank=True, default=None)
    followed_at = models.DateTimeField(null=True, blank=True, default=None)
    unfollowed_at = models.DateTimeField(null=True, blank=True, default=None)

    def __str__(self):
        return self.username

    def get_media(self):
        if not self.media:
            return []

        return json.loads(self.media)

ACTION_TYPE_CHOICE = {
    'like': 'like',
    'follow': 'follow',
}
class Action(models.Model):
    campaign = models.ForeignKey(Campaign)

    typ = models.CharField(choices=ACTION_TYPE_CHOICE.items(), max_length=20)
    source_id = models.CharField(max_length=255)
    comment = models.CharField(max_length=255, default='')

    created_at = models.DateTimeField(auto_now_add=True)
    rollbacked_at = models.DateTimeField(default=None, null=True, blank=True)

class AccountLog(models.Model):
    account = models.ForeignKey(Account)
    count_follows = models.PositiveIntegerField(default=0)
    count_followers = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

TASK_STATUS_CHOICE = {
    0: 'NEW',
    1: 'EXECUTE',
    2: 'SUCCESS',
    3: 'FAILURE',
    4: 'KILL',
    5: 'KILLED',
}

class Task(models.Model):

    name = models.TextField(null=True, blank=True)
    command = models.TextField()
    arguments = models.TextField(null=True, blank=True)

    out = models.TextField(null=True, blank=True)
    err = models.TextField(null=True, blank=True)
    status_string = models.TextField(null=True, blank=True)
    uid = models.CharField(max_length=30)
    status = models.IntegerField(default=0,choices=TASK_STATUS_CHOICE.items(),null=False,blank=False)

    NEW = 0
    EXECUTE = 1
    SUCCESS = 2
    FAILURE = 3
    KILL = 4
    KILLED = 5

    pid = models.PositiveSmallIntegerField(blank=True,null=True)
    try_count = models.PositiveSmallIntegerField(default=0,blank=False,null=False)
    max_try_count = models.PositiveSmallIntegerField(default=None,blank=True,null=True)
    can_retry = models.BooleanField(default=False)
    time_shift = models.PositiveSmallIntegerField(default=None,blank=True,null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    start_at = models.DateTimeField(default=None, null=True, blank=True)

    def can_start(self):
        # если время не указано или время наступило
        if self.start_at == None or timezone.now() > self.start_at:
            # если задача новая
            if self.status == Task.NEW:
                return True
            # если задача неудачно завершилась и может быть перезапущена
            if self.status in [Task.FAILURE,Task.SUCCESS] and self.can_retry:
                from datetime import timedelta
                # if True:
                if self.time_shift == None \
                    or self.updated_at < timezone.now() - timedelta(minutes=self.time_shift):
                    # если указано количество попыток, ограничиваемся
                    if self.max_try_count == None:
                        return True
                    if self.try_count <= self.max_try_count:
                        return True

        return False

    def save_status_string(self, d):
        import json
        self.status_string = json.dumps(d)
        self.save()

    def get_command(self):
        return "python %s/manage.py %s %d" % (settings.BASE_DIR, self.command, self.id)

    def get_argument(self, key, default=None):
        import json
        try:
            arguments = json.loads(self.arguments)
        except:
            return default
        return arguments.get(key, default)

    def is_active_proc(self):
        import os
        if not self.pid:
            return False
        if len(os.popen("ps -ax | grep '^\s*%s'" % self.pid).read().strip()):
            return True
        else:
            return False

    def restart(self):
        self.pid = None
        self.out = None
        self.err = None
        self.status = Task.NEW
        self.status_string = None
        self.save()

    def save(self, *args, **kwargs):
        import json
        if isinstance(self.arguments, dict):
            self.arguments = json.dumps(self.arguments)
        
        return super(Task, self).save(*args, **kwargs)

    def run_now(self):
        subprocess.Popen(
            [
                sys.executable, '%s/private/manage.py' % settings.ROOT_DIR, 
                'run_task', '--task', str(self.id)
            ], 
            stdout=subprocess.PIPE, 
            stderr=subprocess.STDOUT
        )

    def kill(self):
        self.status = Task.KILL
        self.pid = None
        self.save()

        os.popen("kill -9 %s" % self.pid)

    def __str__(self):
        return '%d %s' % (self.id, self.name)