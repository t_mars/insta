# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20150831_1920'),
    ]

    operations = [
        migrations.AddField(
            model_name='attracteduser',
            name='media',
            field=models.TextField(default=None, null=True, blank=True),
        ),
    ]
