# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_accountlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='user_id',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
