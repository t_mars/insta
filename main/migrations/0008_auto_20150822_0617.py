# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20150821_2143'),
    ]

    operations = [
        migrations.RenameModel('Task', 'Campaign'),
        migrations.RenameField('Action', 'task', 'campaign'),
        migrations.RenameField('AttractedUser', 'task', 'campaign'),
    ]
