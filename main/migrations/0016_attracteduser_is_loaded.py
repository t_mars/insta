# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_attracteduser_biography'),
    ]

    operations = [
        migrations.AddField(
            model_name='attracteduser',
            name='is_loaded',
            field=models.NullBooleanField(default=None),
        ),
    ]
