# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20150822_0617'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(null=True, blank=True)),
                ('command', models.TextField()),
                ('arguments', models.TextField(null=True, blank=True)),
                ('out', models.TextField(null=True, blank=True)),
                ('err', models.TextField(null=True, blank=True)),
                ('status_string', models.TextField(null=True, blank=True)),
                ('uid', models.CharField(max_length=30)),
                ('status', models.IntegerField(default=0, choices=[(0, b'NEW'), (1, b'EXECUTE'), (2, b'SUCCESS'), (3, b'FAILURE'), (4, b'KILL'), (5, b'KILLED')])),
                ('pid', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('try_count', models.PositiveSmallIntegerField(default=0)),
                ('max_try_count', models.PositiveSmallIntegerField(default=None, null=True, blank=True)),
                ('can_retry', models.BooleanField(default=False)),
                ('time_shift', models.PositiveSmallIntegerField(default=None, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('start_at', models.DateTimeField(default=None, null=True, blank=True)),
            ],
        ),
    ]
