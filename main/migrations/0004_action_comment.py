# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_attracteduser_is_attracted'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='comment',
            field=models.CharField(default=b'', max_length=255),
        ),
    ]
