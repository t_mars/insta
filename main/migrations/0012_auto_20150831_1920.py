# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_remove_attracteduser_is_attracted'),
    ]

    operations = [
        migrations.AddField(
            model_name='attracteduser',
            name='fullname',
            field=models.CharField(default=None, max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='attracteduser',
            name='is_private',
            field=models.NullBooleanField(default=None),
        ),
        migrations.AddField(
            model_name='attracteduser',
            name='url',
            field=models.CharField(default=None, max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='attracteduser',
            name='vk_id',
            field=models.CharField(default=None, max_length=255, null=True, blank=True),
        ),
    ]
