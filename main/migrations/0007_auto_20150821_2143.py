# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_account_user_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='rollbacked_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='action',
            name='typ',
            field=models.CharField(max_length=20, choices=[(b'follow', b'follow'), (b'like', b'like')]),
        ),
    ]
