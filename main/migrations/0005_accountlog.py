# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_action_comment'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count_follows', models.PositiveIntegerField(default=0)),
                ('count_followers', models.PositiveIntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('account', models.ForeignKey(to='main.Account')),
            ],
        ),
    ]
