# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_attracteduser_media'),
    ]

    operations = [
        migrations.AddField(
            model_name='attracteduser',
            name='loaded_at',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
