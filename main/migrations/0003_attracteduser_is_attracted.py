# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_attracteduser_attracted_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='attracteduser',
            name='is_attracted',
            field=models.NullBooleanField(default=None),
        ),
    ]
