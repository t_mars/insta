# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=255)),
                ('password', models.CharField(max_length=255)),
                ('access_token', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('typ', models.CharField(max_length=20, choices=[(b'follow', b'follow'), (b'unfollow', b'unfollow'), (b'like', b'like'), (b'unline', b'unline')])),
                ('source_id', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='AttractedUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_id', models.CharField(max_length=255)),
                ('username', models.CharField(max_length=255)),
                ('count_media', models.PositiveIntegerField(default=0)),
                ('count_follows', models.PositiveIntegerField(default=0)),
                ('count_followers', models.PositiveIntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('followed_at', models.DateTimeField(default=None, null=True, blank=True)),
                ('unfollowed_at', models.DateTimeField(default=None, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('account', models.ForeignKey(to='main.Account')),
            ],
        ),
        migrations.AddField(
            model_name='attracteduser',
            name='task',
            field=models.ForeignKey(to='main.Task'),
        ),
        migrations.AddField(
            model_name='action',
            name='task',
            field=models.ForeignKey(to='main.Task'),
        ),
    ]
