# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20150831_1841'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attracteduser',
            name='is_attracted',
        ),
    ]
