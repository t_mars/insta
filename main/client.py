#coding=utf8
from grab import Grab
from instagram.client import InstagramAPI
import json, urllib2

class AuthException(Exception):
	pass

class BanException(Exception):
	pass

class TimeoutException(Exception):
	pass

class InstagramClient:
	
	def __init__(self, username, password, access_token):
		# init api
		client_secret = '74b473a1616b4ff1aa68331af2ce6b0a'
		client_id = 'f4094c00160c4d0cbb7e6e92ca89334b'
		self.api = InstagramAPI(access_token=access_token, client_secret=client_secret, client_id=client_id)

		# init grab
		self.grab = Grab()
		self.grab.go('https://instagram.com/accounts/login/')

		self.grab_init()
		self.grab.setup(post={
			'username':username,
			'password':password,
		})
		self.grab.go('https://instagram.com/accounts/login/ajax/')
		
		try:
			data = json.loads(self.grab.doc.body)
		except:
			print self.grab.doc.body
			raise AuthException('Unknow error.')

		if not data['authenticated']:
			raise AuthException('Incorrect login/password.')

		self.user_id = self.get_cookie('ds_user_id')
		self.users = {}
	
	def get_user_id(self, username):
		url = 'http://jelled.com/ajax/instagram?do=username&username=%s' % username
		response = urllib2.urlopen(url).read()

		try:
			data = json.loads(response)
		except:
			print response
			return None

		try:
			return data['data'][0]['id']
		except:
			return None

	def like(self, media_id):
		self.grab_init()
		self.grab_go('https://instagram.com/web/likes/%s/like/' % media_id)

		return self.grab_result()

	def unlike(self, media_id):
		self.grab_init()
		self.grab_go('https://instagram.com/web/likes/%s/unlike/' % media_id)
		
		return self.grab_result()

	def follow(self, user_id):
		self.grab_init()
		self.grab_go('https://instagram.com/web/friendships/%s/follow/' % user_id)

		return self.grab_result()

	def unfollow(self, user_id):
		self.grab_init()
		self.grab_go('https://instagram.com/web/friendships/%s/unfollow/' % user_id)
		
		return self.grab_result()

	def get_followers(self, user_id=None):
		users, next_ = self.api.user_followed_by(user_id=user_id or self.user_id)
		
		while next_:
			more_users, next_ = self.api.user_followed_by(with_next_url=next_)
			users.extend(more_users)

			return users

	def get_follows(self, user_id=None):
		users, next_ = self.api.user_follows(user_id=user_id or self.user_id)
		while next_:
			more_users, next_ = self.api.user_follows(with_next_url=next_)
			users.extend(more_users)

		return users

	def get_medias(self, user_id=None, count=None):
		medias, next_ = self.api.user_recent_media(user_id=user_id or self.user_id)
		while next_:
			if count and len(medias) >= count:
				break
			more_medias, next_ = self.api.user_recent_media(with_next_url=next_)
			medias.extend(more_medias)

		return medias

	def get_likes(self, media_id):
		return self.api.media_likes(media_id)
	
	def get_comments(self, media_id):
		return self.api.media_comments(media_id)

	def get_user(self, user_id=None,reset=False):
		if user_id not in self.users or reset:
			self.users[user_id] = self.api.user(user_id)
		
		return self.users[user_id]

	def get_cookie(self, name):
		value = None
		for item in self.grab.cookies.get_dict():
			if item['name'] == name:
				value = item['value']
		
		return value

	def grab_go(self, url):
		try:
			self.grab.go(url)
			print 'url =',url
		except:
			raise TimeoutException()

	def grab_init(self):
		headers = {
			'X-CSRFToken':self.get_cookie('csrftoken'),
			'X-Instagram-AJAX':'1',
			'X-Requested-With':'XMLHttpRequest',
		}
		print 'headers =', headers
		self.grab.setup(method='POST', headers=headers)
		# self.grab.setup(proxy='136.243.84.198:9004', proxy_type='socks5')

	def grab_result(self):
		body = self.grab.doc.body
		
		try:
			data = json.loads(body)
			return data['status'] == 'ok'
		
		except Exception as e:
			print 'body =',body
			print 'code =',self.grab.response.code
			raise BanException('Many queries.')

		return None