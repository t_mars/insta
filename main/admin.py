#coding=utf8
from django.contrib import admin
from django.contrib.admin.filters import SimpleListFilter

from main.models import *

admin.site.register(Account)

class NullFilterSpec(SimpleListFilter):
    title = u''

    parameter_name = u''

    def lookups(self, request, model_admin):
        return (
            ('1', 'Has value', ),
            ('0', 'None', ),
        )

    def queryset(self, request, queryset):
        kwargs = {
        '%s'%self.parameter_name : None,
        }
        if self.value() == '0':
            return queryset.filter(**kwargs)
        if self.value() == '1':
            return queryset.exclude(**kwargs)
        return queryset

class loaded_at_NullFilterSpec(NullFilterSpec):
    title = u'Is Loaded'
    parameter_name = u'loaded_at'

class vk_id_NullFilterSpec(NullFilterSpec):
    title = u'VK ID'
    parameter_name = u'vk_id'

class AttractedUserAdmin(admin.ModelAdmin):
    list_display = ['id', '_username', 'vk_id', 'is_private', 'count_media', 'count_followers', 'count_follows', 
    'created_at', 'loaded_at', 'attracted_at', 'campaign', 'followed_at']
    list_filter = ['followed_at', vk_id_NullFilterSpec, 'is_loaded', 'campaign', 'is_private']
    search_fields = ['username']
    def _username(self, obj):
        return '<a target="_blank" href="https://instagram.com/%s">%s</a>' % (obj.username, obj.username)
    _username.allow_tags = True
admin.site.register(AttractedUser, AttractedUserAdmin)

class CampaignAdmin(admin.ModelAdmin):
    list_display = ['name', 'stat']

    def stat(self, obj):
        s = obj.get_stat()
        if not s:
            return '-'
        return """
            Подписались = %d<br>
            Подписок = %d<br>
            Выхлоп = %2.2f<br>
            ---<br>
            Подписывающихся в час = %2.2f<br>
            Подписок в час = %2.2f<br>
            ---<br>
            Всего подписались = %d<br>
            Всего подписок = %d
        """ % (
            s['new_followers'], 
            s['new_follows'], 
            s['percent'],
            s['new_followers_per_minute'] * 60.0, 
            s['new_follows_per_minute'] * 60.0, 
            s['followers'], 
            s['follows']
        )

    stat.allow_tags = True


admin.site.register(Campaign, CampaignAdmin)

class ActionAdmin(admin.ModelAdmin):
    list_display = ['typ', 'source_id', 'created_at', 'rollbacked_at', '_comment']

    def _comment(self, obj):
        if obj.comment:
            return '<a href="%s">%s</a>' % (obj.comment, obj.comment)
        return '-'
    _comment.allow_tags = True

admin.site.register(Action, ActionAdmin)

class AccountLogAdmin(admin.ModelAdmin):
    list_display = ['account', 'count_follows', 'count_followers', 'created_at']
admin.site.register(AccountLog, AccountLogAdmin)

class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'uid', 'pid', 'try_count', 'can_retry', 'is_active_proc',
        'status', 'status_string', 'created_at', 'updated_at')
    actions = ['restart', 'kill']

    def restart(self, request, queryset):
        for task in queryset:
            task.restart()
            task.run_now()
    restart.short_description = u"Перезапустить"

    def kill(self, request, queryset):
        for task in queryset:
            task.kill()
    kill.short_description = u"Завершить"

    def is_active_proc(self, obj):
        return obj.is_active_proc()
    is_active_proc.boolean = True


admin.site.register(Task, TaskAdmin)